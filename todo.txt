This file holds a list of thought of or planned improvements for this website.

Priority
- Add a search input for skills
- Add list of what I accomplished on the job
- Add links to resources such as github and gitlab

Planned
- Add a resume downloader based on current website information
- Remove information from UI files to a data/constants file
- Refactor similar dropdown lists into one reusable component

Potential
- Add years used to skills. For a quick estimate, include the sum of the years at the jobs that required use of those skills
- Add more descriptions of skills in my own words
- Add skills per company
- Add a second and less formal screen which can include hobbies, interesting facts, and other information that someone may not want to review immidiately
- Add a hobbies section to secondary screen