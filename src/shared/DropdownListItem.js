import {
  makeStyles,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Collapse,
} from "@material-ui/core";
import React from "react";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  list: {
    width: "100%",
  },
  nested: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}));

function DropdownListItem(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(!open);
  };
  return (
    <>
      <ListItem button onClick={handleClick}>
        <ListItemAvatar>
          <Avatar>{props.avatarIcon}</Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={props.primaryText}
          secondary={props.secondaryText}
        />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <div className={classes.nested}>{props.children}</div>
      </Collapse>
    </>
  );
}
DropdownListItem.defaultProps = {
  avatarIcon: null,
  primaryText: null,
  secondaryText: null,
};
export default DropdownListItem;
