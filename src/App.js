import React from "react";

import { Box, Container, Grid, makeStyles } from "@material-ui/core";
import { blue, deepPurple } from "@material-ui/core/colors";

import Introduction from "./dashboard/Introduction";
import PastWork from "./dashboard/PastWork";
import PersonalProjects from "./dashboard/PersonalProjects";
import Education from "./dashboard/Education";
import AboutMe from "./dashboard/AboutMe";
import Skills from "./dashboard/Skills";

const useStyles = makeStyles((theme) => ({
  root: {
    background: `linear-gradient(to right bottom, ${blue[300]}, ${deepPurple[300]})`,
    minHeight: "100vh",
  },
  padding: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  divider: {
    padding: theme.spacing(2),
  },
  fullColumn: {
    maxHeight: '100%',
  }
}));

function App() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Container maxWidth="md" className={classes.padding}>
        <Grid container justify="center" spacing={2}>
          <Grid item xs={12}>
            <Introduction />
          </Grid>
          <Grid item xs={12}>
            <AboutMe />
          </Grid>
          <Grid item xs={12} lg={8}>
            <Education />
            <Box p={1} />
            <PastWork />
            <Box p={1} />
            <PersonalProjects />
          </Grid>
          <Grid item xs={12} lg={4}>
            <Skills />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default App;
