import { makeStyles, Paper, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(4),
    textAlign: "center",
  },
}));
function Introduction(props) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h3">
          Leslie Tilton
        </Typography>
        <Typography variant="h4">Software Engineer</Typography>
      </Paper>
    </div>
  );
}

export default Introduction;
