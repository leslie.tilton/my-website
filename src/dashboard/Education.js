import React from "react";
import {
  makeStyles,
  Paper,
  Typography,
  List,
  Divider,
} from "@material-ui/core";
import SchoolIcon from "@material-ui/icons/School";

import DropdownListItem from "../shared/DropdownListItem";
import constants from "../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  list: {
    width: "100%",
  },
}));
function Education(props) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Education
        </Typography>
        <List className={classes.list}>
          {constants.EDUCATION.map((education, i) => (
            <React.Fragment key={education.degree}>
              <DropdownListItem
                avatarIcon={<SchoolIcon />}
                primaryText={education.degree}
                secondaryText={education.location}
              >
                <Typography variant="subtitle2">{education.date}</Typography>
                <Typography>{education.extra}</Typography>
                {i < constants.EDUCATION.length - 1 && <Divider />}
              </DropdownListItem>
            </React.Fragment>
          ))}
        </List>
      </Paper>
    </div>
  );
}

export default Education;
