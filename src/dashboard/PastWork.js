import React from "react";
import {
  makeStyles,
  Paper,
  Typography,
  List,
  Divider,
} from "@material-ui/core";
import BusinessIcon from "@material-ui/icons/Business";

import DropdownListItem from "../shared/DropdownListItem";
import constants from "../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  list: {
    width: "100%",
  },
}));

function PastWork(props) {
  const classes = useStyles();

  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Work History
        </Typography>
        <List className={classes.list}>
          {constants.PREVIOUS_WORK.map((previousWork, i) => (
            <React.Fragment key={previousWork.company}>
              <DropdownListItem
                avatarIcon={<BusinessIcon />}
                primaryText={previousWork.company}
                secondaryText={previousWork.location}
              >
                <Typography component="div" variant="subtitle2">
                  {previousWork.date}
                </Typography>
                <Typography component="div">{previousWork.title}</Typography>
              </DropdownListItem>
              {i < constants.PREVIOUS_WORK.length - 1 && <Divider />}
            </React.Fragment>
          ))}
        </List>
      </Paper>
    </div>
  );
}

export default PastWork;
