import { makeStyles, Paper, Typography } from "@material-ui/core";
import React from "react";
import constants from "../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
}));
function AboutMe(props) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          About Me
        </Typography>
        <Typography style={{ whiteSpace: "pre-wrap" }}>
          {constants.ABOUT_ME}
        </Typography>
      </Paper>
    </div>
  );
}

export default AboutMe;
