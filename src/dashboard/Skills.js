import React from "react";
import {
  makeStyles,
  Paper,
  Typography,
  List,
  Divider,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import constants from "../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  list: {
    width: "100%",
    maxHeight: 500,
    overflowY: "auto",
  },
}));

function Skills(props) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Skills
        </Typography>
        <List className={classes.list} dense>
          {constants.SKILLS.sort((a, b) =>
            a.toLocaleLowerCase() < b.toLocaleLowerCase() ? -1 : 1
          ).map((skill, i) => (
            <React.Fragment key={skill}>
              <ListItem>
                <ListItemText>{skill}</ListItemText>
              </ListItem>
              {i < constants.SKILLS.length - 1 && <Divider />}
            </React.Fragment>
          ))}
        </List>
      </Paper>
    </div>
  );
}

export default Skills;
