import React from "react";
import {
  makeStyles,
  Paper,
  Typography,
  List,
  Divider,
} from "@material-ui/core";
import BusinessIcon from "@material-ui/icons/Business";

import DropdownListItem from "../shared/DropdownListItem";
import constants from "../constants";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
}));

function PersonalProjects(props) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Personal Projects
        </Typography>
        <List className={classes.list}>
          {constants.PERSONAL_PROJECTS.map((project, i) => (
            <React.Fragment key={project.title}>
              <DropdownListItem
                avatarIcon={<BusinessIcon />}
                primaryText={project.title}
                secondaryText={project.description}
              >
                <Typography component="div">{project.information}</Typography>
                {i < constants.PERSONAL_PROJECTS.length - 1 && <Divider />}
              </DropdownListItem>
            </React.Fragment>
          ))}
        </List>
      </Paper>
    </div>
  );
}

export default PersonalProjects;
