// Holds constants that might be used in multiple places in the app.
// Also serves as a single place for updating information
const ABOUT_ME = `I have always had a strong passion for computers. As soon as my father explained the basic terminal commands and showed me how to place a button on the screen with Visual Basic, I was hooked. I ended up creating a maze game from just showing and hiding buttons!\n\nAlthough my career has lead me to grow my skills in web development using Java and Javascript instead of Visual Basic, I have continued to follow my passion for development. I am constantly working to increase my knowledge andexperience as a developer.`;
const SKILLS = [
  "Jest",
  "Mocha",
  "Chai",
  "Enzyme",
  "Jenkins",
  "Trello",
  "Jira",
  "Git",
  "Scrum Developer",
  "Agile",
  "JavaScript",
  "Java",
  "Python",
  "React.js",
  "Webpack",
  "Grunt",
  "Gulp",
  "Node.js",
  "Oracle SQL",
  "CSS",
  "Bootstrap",
  "Semantic-UI",
  "Material UI",
  "Material Design",
];
const EDUCATION = [
  {
    degree: "Bachelor of Science in Computer Science",
    location: "Louisiana Tech University, Ruston LA",
    date: "2014 - 2018",
    extra: "Concentration in Graphics and Game Design",
  },
];
const PREVIOUS_WORK = [
  {
    company: "Bont Software & Control Systems, Inc.",
    location: "Corinth, Texas",
    date: "2018 - Present",
    title: "Computer Scientist",
    accomplished: null,
  },
  {
    company: "Fenway Group",
    location: "Ruston, Louisiana",
    date: "2016 - 2018",
    title: "Associate Consultant",
    accomplished: null,
  },
];
const PERSONAL_PROJECTS = [
  {
    title: "My Website",
    description: "The website you are currently viewing",
    information:
      "Created in 2020 using react.js. The code is available on my gitlab account.",
  },
];
export default {
  ABOUT_ME: ABOUT_ME,
  EDUCATION: EDUCATION,
  SKILLS: SKILLS,
  PERSONAL_PROJECTS: PERSONAL_PROJECTS,
  PREVIOUS_WORK: PREVIOUS_WORK,
};
